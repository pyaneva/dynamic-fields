function saveData() {
  let name = $('#name').val();
  let type = $('#type').val();
  let dynamicFields = [];
  $('#dynamic-fields :input[name][value]').each(function () {
    dynamicFields[$(this).attr('name')] = $(this).val();
  });


  console.log(dynamicFields)
  return $.ajax({
    type: "post",
    url: "save-data.php",
    data: {
      name, type, dynamicFields
    },

    success: function (response) {
      if (response.success) {
        $.notify({ message: 'Saved' }, { type: 'success' });
      } else {
        $.notify({ message: 'Unsaved' }, { type: 'danger' });
      }
    },

    error: function (error) {
      console.log(error)
    }
  });
}
function getData(field = 'invoice') {

  return $.ajax({
    type: "get",
    url: "get-fields.php",
    data: { "field": field },

    success: function (response) {
      console.log('response', response)
    },

    error: function (error) {
      console.log(error)
    }
  });
}
function generateField(field) {
  let newField = $('<input>')

  let group = $('<div>')
  group.addClass('form-group')

  let label = $('<label>')
  label.text(field.label)

  if (field.type == 'textarea') {
    newField = $('<textarea>')
  } else {
    newField.attr('type', field.type)
  }

  newField.attr('name', field.name)
  newField.attr('value', '')
  newField.addClass('form-control')

  group.append(label)
  group.append(newField)

  $('#dynamic-fields').append(group)
}
function typeChange(selectedValue) {
  let fields = getData(selectedValue).then((res) => {
    $('#dynamic-fields').empty()
    for (let field of res) {
      generateField(field)
    }
  })
}

$(document).ready(function () {
  typeChange($("#type").val());

  $('#submit').click(function (e) {
    console.log('click')
    let fieldCheck = true;
    $('input').each(function () {
      if (!$(this).val()) {
        fieldCheck = false;
        $.notify({ message: `Field ${$(this).attr('name')} can not be empty` }, { type: 'danger' });
      }

    });
    if (fieldCheck) {
      saveData();
    }
  })

  $('#type').change(function () {
    typeChange($(this).val());
  })
});
