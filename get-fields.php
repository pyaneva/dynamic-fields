<?php

$connection = mysqli_connect('localhost', 'root', '', 'dynamic_fields', '3306');

if (!$connection) {
    echo 'No connection';
    exit;
}

$field = $_GET['field'];

$sql = "SELECT * FROM fields WHERE field = '" . $field. "'";
$query = mysqli_query($connection, $sql);
$fields = mysqli_fetch_all($query, MYSQLI_ASSOC);

header('Content-Type: application/json');
echo json_encode($fields);