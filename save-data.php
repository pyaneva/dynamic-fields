<?php
$connection = mysqli_connect('localhost', 'root', '', 'dynamic_fields', '3306');

if (!$connection) {
    echo 'No connection';
    exit;
}

$name = $_POST['name'];
$type = $_POST['type'];

$dynamic_values = [];
foreach ($_POST as $key => $value) {
    if ($key != 'name' || $key != 'type') {
        $dynamic_values[$key] = $value;
    }
}

$dynamic_values = json_encode($dynamic_values, true);
$sql = 'INSERT INTO form_submissions (name, type, dynamic_values) VALUES ("'. $name .'", "'. $type .'", "'. mysqli_real_escape_string($connection, $dynamic_values) .'")';
$query = mysqli_query($connection, $sql);

header('Content-Type: application/json');
if ($query) {
    echo json_encode(['success' => true, 'message' => 'Saved']);
} else {
    echo json_encode(['success' => false, 'message' => 'There was a problem']);
}