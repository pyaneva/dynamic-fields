-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time:  9 апр 2020 в 11:30
-- Версия на сървъра: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dynamic_fields`
--

-- --------------------------------------------------------

--
-- Структура на таблица `fields`
--

CREATE TABLE `fields` (
  `id` int(11) NOT NULL,
  `field` varchar(250) NOT NULL,
  `name` varchar(250) NOT NULL,
  `label` varchar(250) NOT NULL,
  `type` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Схема на данните от таблица `fields`
--

INSERT INTO `fields` (`id`, `field`, `name`, `label`, `type`) VALUES
(1, 'invoice', 'price', 'Price', 'number'),
(2, 'invoice', 'company_name', 'Company name', 'text'),
(3, 'invoice', 'vat_percent', 'Vat percent', 'number'),
(4, 'message', 'for', 'For', 'text'),
(5, 'message', 'message', 'Message', 'textarea'),
(6, 'application', 'city', 'City', 'text'),
(7, 'application', 'building', 'Building', 'text'),
(8, 'application', 'street', 'Street', 'text');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `fields`
--
ALTER TABLE `fields`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `fields`
--
ALTER TABLE `fields`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
