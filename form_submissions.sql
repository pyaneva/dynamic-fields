-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 17 апр 2020 в 16:36
-- Версия на сървъра: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dynamic_fields`
--

-- --------------------------------------------------------

--
-- Структура на таблица `form_submissions`
--

CREATE TABLE `form_submissions` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `type` varchar(250) NOT NULL,
  `dynamic_values` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Схема на данните от таблица `form_submissions`
--

INSERT INTO `form_submissions` (`id`, `name`, `type`, `dynamic_values`) VALUES
(1, 'Angel Miladinov', 'invoice', NULL),
(2, 'Angel Miladinov', 'invoice', ''),
(3, 'Angel Miladinov', 'invoice', '{\"name\":\"Angel Miladinov\",\"type\":\"invoice\",\"price\":\"240\",\"company_name\":\"Test OOD\",\"vat_percent\":\"20\",\"undefined\":\"\"}'),
(4, 'Angel Miladinov', 'invoice', '{\"name\":\"Angel Miladinov\",\"type\":\"invoice\",\"price\":\"240\",\"company_name\":\"Test OOD\",\"vat_percent\":\"20\",\"undefined\":\"\"}');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `form_submissions`
--
ALTER TABLE `form_submissions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `form_submissions`
--
ALTER TABLE `form_submissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
